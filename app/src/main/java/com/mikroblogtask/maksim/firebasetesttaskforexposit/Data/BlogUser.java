package com.mikroblogtask.maksim.firebasetesttaskforexposit.Data;

public class BlogUser {
    private String id;
    private String blogs;
    private String date;

    public BlogUser(String id, String blogs, String date) {
        this.id = id;
        this.blogs = blogs;
        this.date = date;
    }

    public BlogUser(String id, String blogs) {
        this.id = id;
        this.blogs = blogs;
    }

    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public BlogUser() {


    }

    public String getBlogs() {
        return blogs;
    }

    public void setBlogs(String blogs) {
        this.blogs = blogs;
    }
}
