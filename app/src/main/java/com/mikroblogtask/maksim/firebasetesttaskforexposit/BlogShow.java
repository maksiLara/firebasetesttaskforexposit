package com.mikroblogtask.maksim.firebasetesttaskforexposit;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.mikroblogtask.maksim.firebasetesttaskforexposit.Adapters.BlogRecyclerAdapter;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.BlogUser;

import java.util.ArrayList;
import java.util.List;


public class BlogShow extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private DatabaseReference myRef;

    private FirebaseUser user;

    private List<BlogUser> mBlogUsersList;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private TextView mDescriptionTextView;
    private LinearLayoutManager mLinearLayoutManager;

    private FirebaseDatabase mFirebaseDatabase;
    public static final String USER_ID = "userId";

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawernavigation_with_listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getDrawerLayout(toolbar);

        mDescriptionTextView = findViewById(R.id.description_blog_textview_recycler);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        mFirebaseDatabase = FirebaseDatabase.getInstance();

        Intent intent = getIntent();
        String id = intent.getStringExtra(RegistrationForm.USER_ID);
        myRef = mFirebaseDatabase.getReference("Blog").child(id);

        mBlogUsersList = new ArrayList<>();
        mRecyclerView = findViewById(R.id.list_view_list_blog);

        mLinearLayoutManager = new LinearLayoutManager(BlogShow.this);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

    }

    private void getDrawerLayout(Toolbar toolbar) {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_list_blogs);
        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_views);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                updateUIBlog(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateUIBlog(@NonNull DataSnapshot dataSnapshot) {
        mBlogUsersList.clear();
        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            BlogUser blogUser = ds.getValue(BlogUser.class);
            mBlogUsersList.add(blogUser);
        }

        mAdapter = new BlogRecyclerAdapter(mBlogUsersList,BlogShow.this);

        mRecyclerView.setAdapter(mAdapter);
    }

    private void signOut() {
        mAuth.signOut();
        if (user != null) {
            user = null;
            updateUI(user);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBlogUsersList.clear();
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.nav_add_micro_blog:
                menuItem.setChecked(true);
                Intent intent = getIntent();
                String id = intent.getStringExtra(RegistrationForm.USER_ID);
                Intent intent1 = new Intent(BlogShow.this, AddNewMicroBlog.class);
                intent1.putExtra(USER_ID, id);
                startActivity(intent1);
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.nav_profile:
                menuItem.setChecked(true);
                startActivity(new Intent(BlogShow.this, AboutUserProfile.class));
                mDrawerLayout.closeDrawers();
                return true;
            case R.id.nav_logout:
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                Intent intentLogOut = getIntent();
                String idLogOut = intentLogOut.getStringExtra(RegistrationForm.USER_ID);
                Intent intentWriteLogOut = new Intent(BlogShow.this, MainActivity.class);
                intentWriteLogOut.putExtra(USER_ID, idLogOut);
                startActivity(intentWriteLogOut);

                mBlogUsersList.clear();
                signOut();

                return true;

        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                if (item.isChecked()) {
                    item.setChecked(false);
                } else item.setChecked(true);
                return true;
            case R.id.nav_add_micro_blog:
                if (item.isChecked()) {
                    item.setChecked(false);
                } else item.setChecked(true);
                return true;
            case R.id.nav_profile:
                if (item.isChecked()) {
                    item.setChecked(false);
                } else item.setChecked(true);
                return true;
            case R.id.nav_logout:
                if (item.isChecked()) {
                    item.setChecked(false);
                } else item.setChecked(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
