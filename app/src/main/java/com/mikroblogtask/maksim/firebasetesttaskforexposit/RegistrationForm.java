package com.mikroblogtask.maksim.firebasetesttaskforexposit;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.UserInformation;

import java.util.ArrayList;
import java.util.List;

public class RegistrationForm extends BaseActivity {
    private static final String TAG = "CreateAcc";
    public static final String USER_ID = "userId";

    private Button mRegistrationAccountButton;
    private EditText mFirstNameRegistrationEditText;
    private EditText mSurnameRegistrationEditText;
    private EditText mAgeRegistrationEditText;
    private EditText mEmailRegistrationEditText;
    private EditText mPasswordRegistrationEditText;
    private RadioButton mMaleRegistrationRadioBatton;
    private RadioButton mFemaleRegistrationRadioBatton;


    private FirebaseAuth mAuth;
    private FirebaseUser user;
    private DatabaseReference myRef;
    private List<UserInformation> mUserInformations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_form);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
        myRef = FirebaseDatabase.getInstance().getReference("User");

        mRegistrationAccountButton = findViewById(R.id.registrationAccountButton);
        mFirstNameRegistrationEditText = findViewById(R.id.first_name_registration_edit_text_view);
        mSurnameRegistrationEditText = findViewById(R.id.surname_registration_edit_text_view);
        mAgeRegistrationEditText = findViewById(R.id.age_registration_edit_text_view);
        mEmailRegistrationEditText = findViewById(R.id.email_sigin_edit_text_view);
        mPasswordRegistrationEditText = findViewById(R.id.password_signin_edit_text_view);
        mMaleRegistrationRadioBatton = findViewById(R.id.male_registration_radio_button_view);
        mFemaleRegistrationRadioBatton = findViewById(R.id.female_registration_radio_button_view);

        mUserInformations= new ArrayList<>();

        mRegistrationAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount(mEmailRegistrationEditText.getText().toString(), mPasswordRegistrationEditText.getText().toString());
            }
        });
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            writeUserData();
        }
    }

    private void writeUserData() {

        String firstName = mFirstNameRegistrationEditText.getText().toString().trim();
        String surname = mSurnameRegistrationEditText.getText().toString();
        String age = mAgeRegistrationEditText.getText().toString();
        String email = mEmailRegistrationEditText.getText().toString();
        String gender = valueOfGender();
        String id = myRef.push().getKey();

        UserInformation userInformation =new UserInformation(id,firstName,surname,age,gender,email);
        myRef.child(id).setValue(userInformation);

        Intent intent = new Intent(getApplicationContext(),BlogShow.class);

        intent.putExtra(USER_ID,userInformation.getId());
        startActivity(intent);
    }

    @NonNull
    private String valueOfGender() {
        String gender ;
        if (mMaleRegistrationRadioBatton.isChecked()){
            gender= mMaleRegistrationRadioBatton.getText().toString();
        }else {
            gender=mFemaleRegistrationRadioBatton.getText().toString();
        }
        return gender;
    }

    private boolean validateForm() {
        boolean valid = true;
        String email = mEmailRegistrationEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailRegistrationEditText.setError("Required.");
            valid = false;
        } else {
            mEmailRegistrationEditText.setError(null);
        }
        String password = mPasswordRegistrationEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordRegistrationEditText.setError("Required.");
            valid = false;
        } else {
            mPasswordRegistrationEditText.setError(null);
        }
        String firstName = mFirstNameRegistrationEditText.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            mFirstNameRegistrationEditText.setError("Required.");
            valid = false;
        } else {
            mFirstNameRegistrationEditText.setError(null);
        }
        String surname = mSurnameRegistrationEditText.getText().toString();
        if (TextUtils.isEmpty(surname)) {
            mSurnameRegistrationEditText.setError("Required.");
            valid = false;
        } else {
            mSurnameRegistrationEditText.setError(null);
        }
        String age = mAgeRegistrationEditText.getText().toString();
        if (TextUtils.isEmpty(age)) {
            mAgeRegistrationEditText.setError("Required.");
            valid = false;
        } else {
            mAgeRegistrationEditText.setError(null);
        }
        String male = mMaleRegistrationRadioBatton.getText().toString();
        String female = mFemaleRegistrationRadioBatton.getText().toString();
        if (TextUtils.isEmpty(male) || TextUtils.isEmpty(female)) {
            mMaleRegistrationRadioBatton.setError("Required.");
            mFemaleRegistrationRadioBatton.setError("Required");
            valid = false;
        } else {
            mMaleRegistrationRadioBatton.setError(null);
            mFemaleRegistrationRadioBatton.setError(null);
        }
        return valid;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
