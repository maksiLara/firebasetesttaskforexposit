package com.mikroblogtask.maksim.firebasetesttaskforexposit.Adapters;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mikroblogtask.maksim.firebasetesttaskforexposit.R;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.UserInformation;

import java.util.List;

public class UserInformationListAdapter extends ArrayAdapter<UserInformation> {
    private Activity mActivityContext;
    private List<UserInformation> mUserInformations;

    public UserInformationListAdapter(Activity activityContext, List<UserInformation> userInformations) {
        super(activityContext,R.layout.list_user_information,userInformations);
        mActivityContext = activityContext;
        mUserInformations = userInformations;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mActivityContext.getLayoutInflater();
        View listViewItem=inflater.inflate(R.layout.list_user_information,null,true);

        TextView firstNameTextViewList = listViewItem.findViewById(R.id.first_name_user_information_list);
        TextView surNameTextViewList = listViewItem.findViewById(R.id.sur_name_user_information_list);
        TextView ageNameTextViewList = listViewItem.findViewById(R.id.age_name_user_information_list);
        TextView genderNameTextViewList = listViewItem.findViewById(R.id.gender_name_user_information_list);
        TextView emailNameTextViewList = listViewItem.findViewById(R.id.email_name_user_information_list);

        UserInformation userInformation = mUserInformations.get(position);

        firstNameTextViewList.setText(userInformation.getFirstName());
        surNameTextViewList.setText(userInformation.getSurName());
        ageNameTextViewList.setText(userInformation.getAge());
        genderNameTextViewList.setText(userInformation.getGender());
        emailNameTextViewList.setText(userInformation.getEmail());

        return listViewItem;
    }
}
