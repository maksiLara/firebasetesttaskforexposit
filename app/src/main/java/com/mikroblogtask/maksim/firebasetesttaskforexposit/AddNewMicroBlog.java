package com.mikroblogtask.maksim.firebasetesttaskforexposit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.BlogUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AddNewMicroBlog extends AppCompatActivity {

    private EditText mDescriptionMicroBlogEditText;
    private Button mAddNewMicroBlogButton;

    private FirebaseAuth mAuth;
    private DatabaseReference myRef;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_micro_blog);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mDescriptionMicroBlogEditText = findViewById(R.id.description_of_blog_add_new_edittext);
        mAddNewMicroBlogButton = findViewById(R.id.add_new_micro_blog_button);

        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();

        Intent intent = getIntent();
        String id = intent.getStringExtra(RegistrationForm.USER_ID);

        myRef = FirebaseDatabase.getInstance().getReference("Blog").child(id);

        mAddNewMicroBlogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String blogs = mDescriptionMicroBlogEditText.getText().toString();
                String idUser = myRef.push().getKey();
                Date date = new Date();


                String dateOfBlog = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                BlogUser mBlogUser = new BlogUser(idUser, blogs, dateOfBlog);

                myRef.child(idUser).setValue(mBlogUser);
                Intent intent2 = getIntent();
                Intent intent1 = new Intent(AddNewMicroBlog.this, BlogShow.class);
                String idAdd = intent2.getStringExtra(RegistrationForm.USER_ID);
                intent1.putExtra(RegistrationForm.USER_ID, idAdd);
                startActivity(intent1);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}