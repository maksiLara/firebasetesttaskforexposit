package com.mikroblogtask.maksim.firebasetesttaskforexposit.Data;

import java.util.ArrayList;

public class UserInformation {
    private String id;
    private String firstName;
    private String surName;
    private String age;
    private String gender;
    private String email;

    public String getId() {
        return id;
    }

    public UserInformation(String id, String firstName, String surName, String age, String gender, String email) {
        this.id = id;
        this.firstName = firstName;
        this.surName = surName;
        this.age = age;
        this.gender = gender;
        this.email = email;

    }

    public String getSurName() {

        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserInformation() {

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
