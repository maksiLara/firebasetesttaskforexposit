package com.mikroblogtask.maksim.firebasetesttaskforexposit;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends BaseActivity/* implements NavigationView.OnNavigationItemSelectedListener*/ {

    private static final String TAG = "EmailPassword";

    private Button mSignInButton;
    private Button mRegistrationAccountButton;
    private EditText mEmailSignEditText;
    private EditText mPasswordSignEditText;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

        mSignInButton = findViewById(R.id.signInButton);
        mRegistrationAccountButton = findViewById(R.id.registrationAccountButton);
        mPasswordSignEditText = findViewById(R.id.password_signin_edit_text_view);
        mEmailSignEditText = findViewById(R.id.email_sigin_edit_text_view);

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(mEmailSignEditText.getText().toString(), mPasswordSignEditText.getText().toString());
            }
        });

        mRegistrationAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegistrationForm.class));
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {

        } else {

        }
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Intent intent1 = getIntent();
                            String id = intent1.getStringExtra(RegistrationForm.USER_ID);
                            Intent intent = new Intent(MainActivity.this, BlogShow.class);
                            intent.putExtra(RegistrationForm.USER_ID, id);
                            startActivity(intent);

                            updateUI(user);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                        hideProgressDialog();
                    }
                });
    }

    private boolean validateForm() {
        boolean valid = true;
        String email = mEmailSignEditText.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailSignEditText.setError("Required.");
            valid = false;
        } else {
            mEmailSignEditText.setError(null);
        }
        String password = mPasswordSignEditText.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordSignEditText.setError("Required.");
            valid = false;
        } else {
            mPasswordSignEditText.setError(null);
        }
        return valid;
    }
}
