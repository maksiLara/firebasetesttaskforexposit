package com.mikroblogtask.maksim.firebasetesttaskforexposit.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.BlogUser;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.R;

import java.util.List;

public class BlogRecyclerAdapter extends RecyclerView.Adapter<BlogRecyclerAdapter.ViewHolder> {

    private List<BlogUser> mBlogUsers;
    private LayoutInflater mInflater;
    private Activity mActivityContext;

    public BlogRecyclerAdapter(List<BlogUser> blogUsers, Activity context) {
        this.mBlogUsers = blogUsers;
        this.mActivityContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mInflater=mActivityContext.getLayoutInflater();
        View view = mInflater.inflate(R.layout.blogs_list_card,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        BlogUser blogUser = mBlogUsers.get(i);
        viewHolder.mTextView.setText(blogUser.getBlogs());
        viewHolder.mTextViewDate.setText(blogUser.getDate());

    }

    @Override
    public int getItemCount() {
        return mBlogUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public TextView mTextViewDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            mTextView = itemView.findViewById(R.id.description_blog_text_view_adapte);
            mTextViewDate=itemView.findViewById(R.id.date_blog_text_view_adapte);
        }
    }
}
