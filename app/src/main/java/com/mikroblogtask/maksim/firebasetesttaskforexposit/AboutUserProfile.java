package com.mikroblogtask.maksim.firebasetesttaskforexposit;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Adapters.UserInformationListAdapter;
import com.mikroblogtask.maksim.firebasetesttaskforexposit.Data.UserInformation;

import java.util.ArrayList;
import java.util.List;

public class AboutUserProfile extends AppCompatActivity {


    private FirebaseDatabase mFirebaseDatabase;

    private DatabaseReference myRef;

    private ListView mListView;
    private List<UserInformation> mUserInformations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_user_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mListView = findViewById(R.id.user_information_list_view);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        Intent intent = getIntent();
        myRef = mFirebaseDatabase.getReference("User");

        mUserInformations = new ArrayList<>();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mUserInformations.clear();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                updateUI(dataSnapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void updateUI(@NonNull DataSnapshot dataSnapshot) {

        for (DataSnapshot ds : dataSnapshot.getChildren()) {
            mUserInformations.clear();
            UserInformation userInformation = ds.getValue(UserInformation.class);
            mUserInformations.add(userInformation);
        }
        UserInformationListAdapter arrayAdapter = new UserInformationListAdapter(AboutUserProfile.this, mUserInformations);
        mListView.setAdapter(arrayAdapter);
    }


}